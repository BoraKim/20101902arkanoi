#pragma strict

static var speed = 5.5;
// 무적 볼 시간
static var undeadTime = 0.0;

private var pos : Vector3;

function Update () 
{
    if( eStage.READY == jsGameManager.stage 
      || eStage.STAGE == jsGameManager.stage )
      {
        return;
      }

      pos = transform.position;

    //무적 볼 시간 감소
    var amtMove = Time.deltaTime * speed;
    // 이동
    transform.Translate(transform.forward * amtMove, Space.World);

    if( transform.position.z < -12 
        || transform.position.z > 2 )
    {
        jsGameManager.stage = eStage.OUT;
    }

    // 무적 볼
    undeadTime -= Time.deltaTime;
    if( undeadTime > 0 )
    {
        transform.renderer.material.color = Color.red;
    }
    else
    {
        transform.renderer.material.color = Color.white;
    }
}

// 충돌 판정
function OnTriggerEnter(coll : Collider)
{
    // 무적 볼은 무조건 블록 파괴
    var flag = false;

    if( undeadTime > 0 )
    {
        flag = true;
    }


    CheckBounds(coll);

    if( coll.gameObject.tag.Substring(0,5) == "BLOCK"
       && jsGameManager.stage != eStage.DEMO ) 
    {
        coll.gameObject.SendMessage("SetCollision", flag, SendMessageOptions.DontRequireReceiver);
    }
}

//공의 반사
function CheckBounds(coll : Collider)
{
    if( coll.tag == "FENCETOP" )
    {
        transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
        return;
    }
    else if( coll.tag == "FENCE" )
    {
        transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
        return;
    }

    if( undeadTime > 0 )
    {
        return;
    }

    // 충돌 범위
    var bounds : Bounds = coll.bounds;
    // 공의 좌표
    //var pos : Vector3 = transform.position;
    // 공의 반지름
   // var w = transform.localScale.x / 2;

    // 위아래 충돌
    if( pos.x >= (bounds.min.x)
     && pos.x <= (bounds.max.x) )
     {
        transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
        transform.position = pos;
     }
     else
     {
        transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
        transform.position = pos;
     }
}