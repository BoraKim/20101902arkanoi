#pragma strict

private var speed = 8;

function Update () {
    var amtToMove = speed * Time.deltaTime;
    transform.Translate(Vector3.forward * amtToMove);
    if( transform.position.z > 2)
    {
         Destroy(gameObject);
    }
}

//충돌 판정
function OnTriggerEnter(coll : Collider)
{
    if("BLOCK" == coll.gameObject.tag.Substring(0,5))
    {
       coll.gameObject.SendMessage("SetCollision", true, SendMessageOptions.DontRequireReceiver);
    }
    Destroy(gameObject);
}