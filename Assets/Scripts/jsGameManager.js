#pragma strict

// 스테이지 번호
private var stageNum = 1;
var gStageBallManager : GameObject;
var gBackGround : GameObject;

// 패들
var paddle : Transform;
// 공
var ball : Transform;
// 보너스 아이템
var bonus : Transform;
// 스피드 볼
var speedBall : Transform;

// ui오브젝트들
var uiScroe : GUIText;
var uiStage : GUIText;
var uiTryAgin : GUIText;

// 남은 공의 수
static var ballCnt = 5;
// 보너스 아이텥ㅁ 번호
static var bonusNum : int;

// 스테이지 수
var stagecnt : int;

// 외부 모듈과 공용 변수
enum eStage
{ 
    STAGE,
    RESET,
    HIT,
    DESTROY,
    OUT,
    BONUS,
    IDLE,
    READY,
    DEMO,
};

static var stage :eStage = eStage.STAGE;
private var score = 0;
static var blockNum : int;
static var blockPos : Vector3;

function Start () {
    stagecnt = jsStage.Stage[stageNum-1].Length;
    MakeStage();
}

function Update ()
{
    switch(stage)
    {
        case eStage.STAGE:
        MakeStage();
        break;

        case eStage.RESET:
        ResetPosition();
        break;

        case eStage.HIT:
        SetHit();
        break;

        case eStage.DESTROY:
        SetDestroy();
        break;

        case eStage.OUT:
        SetOut();
        break;

        case eStage.BONUS:
        ProcessBonus();
        break;
    }

    //print("Stage = " + stage);
}

// 패들과 공 위치 초기화
function ResetPosition()
{
   	// 스테이지마다 스패들의 길이를 0.15씩 감소
	var padLen =  1.85 - stageNum * 0.15;
	if (padLen < 1.0)
		padLen = 1.0;

	// 볼의 초기 위치는 패들의 위
	paddle.transform.position.x = 0;
	paddle.transform.localScale = Vector3(padLen, 0.25, 0.25);
	
	ball.transform.position = Vector3(0, 0, paddle.transform.position.z + 0.32);
	
	var arAngles = [-30, -45, -60, 60, 45, 30];
	ball.transform.rotation.eulerAngles.y = arAngles[ Random.Range(0, 6) ];

	jsBall.speed = 5.0 + stageNum * 0.5;
	
	stage = eStage.READY;
}

//볼 out 처리
function SetOut()
{
    stage = eStage.RESET;
	jsBall.undeadTime = 0;		// 무적볼 해제
	jsPaddle.laserTime = 0;		// 레이저 발사 해제

	ballCnt--;
	if (ballCnt < 0) { 
		ball.transform.position.z = -10;
		stage = eStage.DEMO;
		ShowMessage();			// Try Again
	}	
}

// 점수처리 - 블록 남아 있음
function SetHit()
{
    score += 100;
    if( jsBall.speed < 10 )
        jsBall.speed += 0.05;
    stage = eStage.IDLE;
}

//블록 파괴함
function SetDestroy()
{  
    stage = eStage.IDLE;
    score += (500 * blockNum);
    if( jsBall.speed < 10)
        jsBall.speed += 0.05;

    if( GetBlockCount() == 0 )
    {
        stageNum++;
        ClearStage();
        stage = eStage.STAGE;   
        return;
    }

    MakeBonus();
}

//남은 블록 수 계산
function GetBlockCount()
{
    var cnt = 0;
    for( var i = 1; i <=4 ; ++i)
    {
        cnt += GameObject.FindGameObjectsWithTag("BLOCK"+1).Length;
    }

    return cnt;
}

//Clear Stage
function ClearStage()
{
    var balls = GameObject.FindGameObjectsWithTag("BALL9");
    for( var obj in balls)
    {
        Destroy(obj);
    }

    for( var i = 1; i <=4 ; ++i)
    {
       var blocks = GameObject.FindGameObjectsWithTag("BLOCK"+1);
       for( obj in blocks)
       {
        Destroy(obj);
       }
    }
}

//보너스 아이템 만들기
function MakeBonus()
{
    var n = Random.Range(0, 100);
    if( n <= 95 ) return;

    var colors = [Color.yellow, Color.green, Color.red, Color.cyan, Color.black];
    n = Random.Range(1,6);

    try     {
		var obj = Instantiate(bonus, blockPos, Quaternion.identity);
		obj.Find("Sphere").renderer.material.color = colors[n - 1];
		obj.Find("Sphere").tag = "BONUS" + n;
    }catch(err)    {
    }
}

// 보너스 아이템 처리
function ProcessBonus()
{
    switch (bonusNum) {
		case 1 :			// yellor 볼의 갯수 증가  
			if (ballCnt < 5)
				ballCnt++;
			break;
		case 2 :			// green 패들 길이를 0.5 길게 
			paddle.transform.localScale.x += 0.5;
			if (paddle.transform.localScale.x > 2.2)
				paddle.transform.localScale.x = 2.2;
			break;
		case 3 :			// red  5초간 무적 볼  
			jsBall.undeadTime = 5;
			break;
		case 4 :			// cyan 10초간 레이저 발사 
			jsPaddle.laserTime = 10;
			break;
		case 5 :			// black 스피드 볼  
			MakeSpeedBall();
			break;
	}
		
	stage = eStage.IDLE;
}

//스피드 볼 처리
function MakeSpeedBall()
{
	var cnt = Random.Range(6, 11);			// 6~10개 만듦
	var pos : Vector3 = paddle.transform.position;
	pos.z += 1f;

	for (var i = 1; i <= cnt; i++) {
		var obj : Transform = Instantiate(speedBall, pos, Quaternion.identity);
		obj.renderer.material.color = Color.black;
		obj.SendMessage("SetSpeed", SendMessageOptions.DontRequireReceiver);
		
		yield WaitForSeconds(0.2);			// 0.2초 간격으로 화면애 배치
	}
}

// ui 표시
function OnGUI()
{
    var w = Screen.width;
    var h = Screen.height;

    for(var i = 1; i<= ballCnt; ++i)
    {
        var x = 22 * 1 - 17;
        var y = h - 15;

        GUI.DrawTexture(Rect(x, y, 20, 10), Resources.Load("Texture/imgPaddle", Texture2D));
    }

    uiStage.text = "Stage : " + stageNum;
    uiScroe.text = "Score : " + score.ToString("n0"); 
}

// 재시도 묻는듯
function ShowMessage()
{
    uiTryAgin.text = "Try Again? (y/n)";
    var w = Screen.width / 2;		
    var h = Screen.height * 0.75;	
	
    do
    {
    	var isYes =Input.GetKeyUp(KeyCode.Y);
	    var isNo = Input.GetKeyUp(KeyCode.N);

        yield 0;
    }while(!isYes&& !isNo);
    
    uiTryAgin.text = "";

     // 점수 등 초기화
	if (isYes) {
		score = 0;
		ballCnt = 3;
		stageNum = 1;
		ClearStage();
		MakeStage();
		
	}
	
	else if (isNo) {
		Application.LoadLevel("main");
	}	
}

function MakeStage()
{
    // 배경
    gBackGround.renderer.material.mainTexture = Resources.Load("Texture/space"+Random.Range(2,6), Texture2D);
    //블록의 시작 위치
    // 맨 왼쪽
    var px = -3.0;
    // 맨 위
    var pz = 0.0;
    // 블록 간 수평 간격
    var w = 1.0;
    // 블록 간 수직 간격
    var h = 0.5;

    // 스테이지의 맵 읽기
    var temp = jsStage.Stage[stageNum-1];

    for(var i = 0; i <temp.Length; ++i)
    {
        var s : String = temp[i];
        var x = px;

        for( var j = 0; j < s.Length; ++j)
        {
            var ch = s.Substring(j,1);

            if( "." == ch || " " == ch )
            {
                //.과 공백은 1칸
                x += w;
                continue;
            }
            else if( "-" == ch )
            {
                // -은 반칸
                x += w /2;
                continue;
            }
            var block : GameObject = Instantiate(Resources.Load("Prefabs/pfBlock"+ch, GameObject));
            block.transform.position = Vector3(x, 0, pz);
            block.transform.parent = gStageBallManager.transform;
            x+=w;
        }
        pz -= h;
    }

    // 패들과 공 위치 초기화
    ResetPosition();
}