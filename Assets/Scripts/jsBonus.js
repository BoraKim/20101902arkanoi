#pragma strict

private var speed = 4;

function Update () 
{
    var amtToMove = speed * Time.deltaTime;
    transform.Translate(Vector3.back * amtToMove);
    if( -12 > transform.position.z )
    {
         Destroy(gameObject);
    }
}

//충돌 판정
function OnTriggerEnter(coll : Collider)
{
    if("PADDLE" == coll.gameObject.tag)
    {
        jsGameManager.bonusNum = int.Parse(gameObject.tag.Substring(5,1));
        Destroy(gameObject);
    }
}