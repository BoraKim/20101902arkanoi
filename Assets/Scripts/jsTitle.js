﻿#pragma strict


var title : Texture;

function OnGUI()
{
    var w = Screen.width;
    var h = Screen.height;

    GUI.DrawTexture(Rect(0, 0, w, h), title);

    if(Input.anyKey)
    {
        Application.LoadLevel("game");
        jsGameManager.stage = eStage.STAGE;
    }
}