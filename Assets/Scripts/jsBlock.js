﻿#pragma strict

var hit = 1;
var explosion : Transform;

// 충돌 후의 처리
function SetCollision(flag : boolean)
{
    if( jsGameManager.stage != eStage.DEMO)
    {
        hit--;
    }
    
    if( flag == true )
    {
        hit -= 1;
    }

    if( hit >= 0)
    {
        jsGameManager.stage = eStage.HIT;
        var color = transform.renderer.material.color;
        transform.renderer.material.color = Color.red;

        yield WaitForSeconds(0.2);
        transform.renderer.material.color = color;
    }
    else
    {
        Instantiate(explosion, transform.position, Quaternion.identity);

        jsGameManager.stage = eStage.DESTROY;
        jsGameManager.blockNum = int.Parse(transform.tag.Substring(5,1));

        jsGameManager.blockPos = transform.position;
        Destroy(gameObject);
    }

}