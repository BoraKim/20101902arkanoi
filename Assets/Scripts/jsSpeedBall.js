#pragma strict

private var speed : float;
private var rotation : float;

function Update () {
    var amtMove = speed * Time.deltaTime;
    transform.Translate(transform.forward * amtMove, Space.World);

    // 공을 잃음
    if( transform.position.z < - 12
        || transform.position.z > 2)
    {
        Destroy(gameObject);
    }
}

//충돌 판정
function OnTriggerEnter(coll : Collider)
{
    CheckBounds(coll);

    if("BLOCK" == coll.gameObject.tag.Substring(0,5) )
    {
        coll.gameObject.SendMessage("SetCollision", true, SendMessageOptions.DontRequireReceiver);
    }
}

//공의 반사
function CheckBounds(coll : Collider)
{
    if( "FENCETOP" == coll.tag)
    {
        transform.rotation.eulerAngles.y = 
                 180 - transform.rotation.eulerAngles.y;
        return;
    }
    else if( coll.tag == "FENCE" )
    {
        transform.rotation.eulerAngles.y =
                 360 - transform.rotation.eulerAngles.y;
        return;
    }

    var bounds : Bounds = coll.bounds;
    var pos : Vector3 = transform.position;

      // 위아래 충돌
    if( Mathf.Abs(bounds.center.x - pos.x ) > (bounds.extents.x) )
     {
        transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
     }
     else
     {
        transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
     }
}

// 공의 속도와 방향 설정
function SetSpeed()
{
    speed = Random.Range(5, 8);
    rotation = Random.Range(-45, 45);
    transform.rotation.eulerAngles.y = rotation;
}